<?php

use yii\db\Migration;

class m230131_151658_create_request_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%request}}', [
            'id' => $this->primaryKey(),
            'customer_full_name' => $this->string()->notNull(),
            'comment' => $this->text()->notNull(),
            'name' => $this->string()->null(),
            'phone' => $this->string()->notNull(),
            'price' => $this->decimal(10,2)->null(),
            'product_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex(
            'idx-request-product_id',
            'request',
            'product_id'
        );
        $this->addForeignKey(
            'fk-request-product_id',
            'request',
            'product_id',
            'product',
            'id',
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-request-product_id', 'request');
        $this->dropIndex('idx-request-product_id', 'request');
        $this->dropTable('{{%request}}');
    }
}
