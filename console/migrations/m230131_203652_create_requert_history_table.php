<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requert_history}}`.
 */
class m230131_203652_create_requert_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%request_history}}', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'diff' => $this->json()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-request_history-request_id',
            'request_history',
            'request_id'
        );
        $this->createIndex(
            'idx-request_history-user_id',
            'request_history',
            'user_id'
        );
        $this->addForeignKey(
            'fk-request_history-request_id',
            'request_history',
            'request_id',
            'request',
            'id',
        );
        $this->addForeignKey(
            'fk-request_history-user_id',
            'request_history',
            'user_id',
            'user',
            'id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-request_history-request_id',
            'request_history'
        );
        $this->dropForeignKey(
            'fk-request_history-user_id',
            'request_history'
        );
        $this->dropIndex(
            'idx-request_history-request_id',
            'request_history'
        );
        $this->dropIndex(
            'idx-request_history-user_id',
            'request_history'
        );
        $this->dropTable('{{%request_history}}');
    }
}
