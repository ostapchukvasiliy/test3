<?php

namespace console\controllers;

use common\foundation\rbac\Permission;
use common\foundation\rbac\Role;
use Exception;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    private const ROLES = [
        [
            'name' => Role::ADMIN,
            'description' => 'Админ',
            'permissions' => [
                Permission::USER_VIEW,
                Permission::USER_CREATE,
                Permission::USER_UPDATE,
                Permission::USER_DELETE,
                Permission::REQUEST_VIEW,
                Permission::REQUEST_CREATE,
                Permission::REQUEST_UPDATE,
                Permission::REQUEST_DELETE,
                Permission::REQUEST_HISTORY_VIEW,
            ],
        ],
        [
            'name' => Role::MANAGER,
            'description' => 'Менеджер',
            'permissions' => [
                Permission::USER_VIEW,
                Permission::REQUEST_VIEW,
                Permission::REQUEST_UPDATE,
                Permission::REQUEST_DELETE,
                Permission::REQUEST_HISTORY_VIEW,
            ],
        ],
        [
            'name' => 'guest',
            'description' => 'Guest',
            'permissions' => [
                Permission::REQUEST_CREATE,
            ],
        ],
    ];

    private const PERMISSIONS = [
        ['name' => Permission::USER_VIEW, 'description' => 'View user'],
        ['name' => Permission::USER_CREATE, 'description' => 'Create user'],
        ['name' => Permission::USER_UPDATE, 'description' => 'Update user'],
        ['name' => Permission::USER_DELETE, 'description' => 'Delete user'],

        ['name' => Permission::REQUEST_VIEW, 'description' => 'View request'],
        ['name' => Permission::REQUEST_CREATE, 'description' => 'Create request'],
        ['name' => Permission::REQUEST_UPDATE, 'description' => 'Update request'],
        ['name' => Permission::REQUEST_DELETE, 'description' => 'Delete request'],

        ['name' => Permission::REQUEST_HISTORY_VIEW, 'description' => 'View request history'],
    ];

    /**
     * @throws Exception
     */
    public function actionFill()
    {
        $this->stdout("Fill roles and permissions\n");

        $auth = Yii::$app->authManager;
        $auth->removeAll();

        foreach (self::PERMISSIONS as $permission) {
            $item = $auth->createPermission($permission['name']);
            $item->description = $permission['description'];
            $auth->add($item);
        }

        foreach (self::ROLES as $role) {
            $item = $auth->createRole($role['name']);
            $item->description = $role['description'];
            $auth->add($item);

            foreach ($role['permissions'] as $permission) {
                $auth->addChild($item, $auth->getPermission($permission));
            }
        }
    }
}
