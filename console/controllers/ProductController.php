<?php

namespace console\controllers;

use common\models\Product;
use Exception;
use yii\console\Controller;

class ProductController extends Controller
{
    private const PRODUCTS = [
        'яблоко',
        'апельсин',
        'мандарин'
    ];

    /**
     * @throws Exception
     */
    public function actionFill(bool $truncate = false)
    {
        if ($truncate) {
            Product::deleteAll();
        }

        foreach (self::PRODUCTS as $name) {
            $product = Product::findOne(['name' => $name]) ?: new Product;
            if ($product->isNewRecord) {
                $product->name = $name;
            }
            $product->save();

            $this->stdout("Fill product: \"$name\"\n");
        }
    }
}
