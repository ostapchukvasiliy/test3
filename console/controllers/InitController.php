<?php

namespace console\controllers;

use yii\console\Controller;

class InitController extends Controller
{
    /**
     * Generate application key
     *
     * @param string $file
     */
    public function actionGenerateAppKey(string $file = '.env')
    {
        $this->stdout("Generate application key in $file...\n");

        $file = APP_PATH . '/' . $file;
        $length = 32;
        $bytes = openssl_random_pseudo_bytes($length);
        $key = strtr(substr(base64_encode($bytes), 0, $length), '+/=', '_-.');
        $content = preg_replace('/(APP_KEY=)(.*)\n/', "\\1$key\n", file_get_contents($file));
        file_put_contents($file, $content);
    }
}
