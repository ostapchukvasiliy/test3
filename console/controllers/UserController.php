<?php

namespace console\controllers;

use backend\modules\user\services\UserService;
use common\foundation\rbac\Role;
use common\models\User;
use Exception;
use yii\console\Controller;

class UserController extends Controller
{
    private UserService $service;

    public function init()
    {
        parent::init();

        $this->service = (new UserService)->skipCheckAccess();
    }

    /**
     * @throws Exception
     */
    public function actionCreateAdmin(string $username, string $email, string $password)
    {
        $this->service->create([
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'roles' => [Role::ADMIN],
            'status' => User::STATUS_ACTIVE,
        ]);

        $this->stdout("Created success.");
    }

    /**
     * @throws Exception
     */
    public function actionDemo(int $count = 20, bool $truncate = true)
    {
        if ($count <= 0) {
            $this->stdout("Nothing create.");
        }
        if ($truncate) {
            User::deleteAll('username like "demo_%"');
        }

        for ($i = 0; $i < $count; $i++) {
            $name = "demo_$i";
            $password = "123456";
            $this->service->create([
                'username' => "$name",
                'password' => $password,
                'email' => "$name@mail.test",
                'roles' => [Role::MANAGER],
                'status' => rand(0,1) ? User::STATUS_ACTIVE : User::STATUS_INACTIVE,
            ]);

            $this->stdout("User created: username \"$name\", password \"$password\".\n");
        }
    }
}
