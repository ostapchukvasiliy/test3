<?php

namespace common\services;

use common\models\RequestForm;
use common\exceptions\InvalidFormException;
use common\foundation\rbac\Permission;
use common\models\Request;
use common\models\RequestHistory;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\data\Pagination;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use Yii;
use Throwable;

class RequestService
{
    const DEFAULT_PER_PAGE = 25;
    const DEFAULT_PAGE = 1;
    const DEFAULT_SORT_BY = 'id';
    const DEFAULT_SORT_ORDER = 'desc';

    /**
     * @throws UnauthorizedHttpException
     */
    public function list(array $data = []): array
    {
        $this->checkAccess(Permission::REQUEST_VIEW);

        $perPage = ((int) $data['perPage'] ?? self::DEFAULT_PER_PAGE);
        $page = ((int) $data['page'] ?? self::DEFAULT_PAGE);
        $sortBy = $data['sort'] ?? self::DEFAULT_SORT_BY;
        $sortOrder = $data['sort'] ?? self::DEFAULT_SORT_ORDER;

        $query = Request::find();
        $pager = new Pagination([
            'totalCount' => (clone $query)->count()
        ]);
        $pager->setPage($page);
        $pager->setPageSize($perPage);

        $users = $query
            ->orderBy([$sortBy => $sortOrder])
            ->offset($pager->getOffset())
            ->limit($pager->getLimit())
            ->all();

        return [
            'data' => $users,
            'pager' => (array) $pager,
        ];
    }

    /**
     * @throws UnauthorizedHttpException
     * @throws NotFoundHttpException
     */
    public function get(?int $id): Request
    {
        $this->checkAccess(Permission::REQUEST_VIEW);

        return $this->getById($id);
    }

    /**
     * @throws UnauthorizedHttpException
     * @throws InvalidFormException
     * @throws Exception
     */
    public function create(array $data): void
    {
        $this->checkAccess(Permission::REQUEST_CREATE);
        RequestForm::validateData($data);

        $request = new Request();
        $request->load($data);
        $this->save($request);
    }


    /**
     * @throws UnauthorizedHttpException
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function update(?int $id, array $data): void
    {
        $this->checkAccess(Permission::REQUEST_UPDATE);
        RequestForm::validateData($data);

        $request = $this->getById($id);
        $request->load($data);
        $this->save($request);
    }

    /**
     * @throws UnauthorizedHttpException
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function changeStatus(?int $id, int $status): void
    {
        $this->checkAccess(Permission::REQUEST_UPDATE);
        $request = $this->getById($id);
        $request->changeStatus($status);
        $this->save($request);
    }

    /**
     * @throws StaleObjectException
     * @throws UnauthorizedHttpException
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function delete(?int $id): void
    {
        $this->checkAccess(Permission::REQUEST_DELETE);
        $request = $this->getById($id);
        $request->delete();
    }

    public function history(int $id, array $data): array
    {
        $this->checkAccess(Permission::REQUEST_HISTORY_VIEW);

        $perPage = ((int) $data['perPage'] ?? self::DEFAULT_PER_PAGE);
        $page = ((int) $data['page'] ?? self::DEFAULT_PAGE);
        $sortBy = $data['sort'] ?? self::DEFAULT_SORT_BY;
        $sortOrder = $data['sort'] ?? self::DEFAULT_SORT_ORDER;

        $request = $this->getById($id);
        $query = RequestHistory::find()->where(['request_id' => $request->id]);
        $pager = new Pagination([
            'totalCount' => (clone $query)->count()
        ]);
        $pager->setPage($page);
        $pager->setPageSize($perPage);

        $history = $query
            ->orderBy([$sortBy => $sortOrder])
            ->offset($pager->getOffset())
            ->limit($pager->getLimit())
            ->all();

        return [
            'request' => $request,
            'data' => $history,
            'pager' => (array) $pager,
        ];
    }

    /**
     * @throws UnauthorizedHttpException
     */
    private function checkAccess(string $permission): void
    {
        if (! Yii::$app->user->can($permission)) {
            throw new UnauthorizedHttpException('Нет доступа.');
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    private function getById(?int $id): Request
    {
        $request = $id ? Request::findOne(['id' => $id]) : null;
        if (! $request) {
            throw new NotFoundHttpException('Заявка не найдена.');
        }
        return $request;
    }

    /**
     * @throws Exception
     */
    private function save(Request $request): void
    {
        if (! $request->validate()) {
            $errors = $request->getFirstErrors();
            throw new InvalidArgumentException(reset($errors));
        }
        $diff = $request->getDirtyAttributes();
        if (! $request->save()) {
            throw new Exception('Ошибка сохранения.');
        }
        if ($diff) {
            $this->saveHistory($request, $diff);
        }
    }

    private function saveHistory(Request $request, array $diff): void
    {
        $history = new RequestHistory();
        $history->user_id = Yii::$app->user->id;
        $history->request_id = $request->id;
        $history->diff = RequestHistory::completeDiff($diff);
        $history->save();
    }

    public function generateCsv(): string
    {
        $headers = implode(';', [
            'Ид.',
            'Наименование',
            'Клиент',
            'Телефон',
            'Комментарий',
            'Цена',
            'Статус',
            'Продукт',
            'Дата создания',
        ]);
        $items = array_map(
            fn(Request $request) => implode(';', [
                $request->id,
                $request->name,
                $request->customer_full_name,
                $request->phone,
                $request->comment,
                $request->price,
                $request->statusName(),
                $request->product->name,
                Yii::$app->formatter->asDatetime($request->created_at),
            ]),
            Request::find()->all()
        );

        array_unshift($items, $headers);

        return implode("\n\r", $items);
    }
}