<?php

namespace common\models;

class RequestForm extends Form
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $comment = null;
    public ?string $customer_full_name = null;
    public ?string $phone = null;
    public ?string $price = null;
    public ?int $product_id = null;
    public ?int $status = null;

    public static function createByRequest(Request $request): RequestForm
    {
        $form = new self;
        $form->id = $request->id;
        $form->name = $request->name;
        $form->comment = $request->comment;
        $form->customer_full_name = $request->customer_full_name;
        $form->phone = $request->phone;
        $form->price = $request->price;
        $form->product_id = $request->product_id;
        $form->status = $request->status;

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            ...$this->rulesComment(),
            ...$this->rulesCustomerName(),
            ...$this->rulesName(),
            ...$this->rulesPhone(),
            ...$this->rulesPrice(),
            ...$this->rulesProductId(),
            ...$this->rulesStatus(),
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id'                 => 'Ид.',
            'customer_full_name' => 'Клиент',
            'name'               => 'Наименование',
            'phone'              => 'Телефон',
            'price'              => 'Цена',
            'product_id'         => 'Продукт',
            'status'             => 'Статус',
            'comment'            => 'Комментарий',
            'created_at'         => 'Дата создания',
            'updated_at'         => 'Дата изменения',
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function getStatusList(): array
    {
        return Request::STATUS_LIST;
    }

    public function getProductList(): array
    {
        return Product::find()
            ->select(['name'])
            ->indexBy('id')
            ->column();
    }

    protected function rulesName(): array
    {
        return [
            ['name', 'string', 'max' => 256],
        ];
    }

    protected function rulesProductId(): array
    {
        return [
            ['product_id', 'required'],
            ['product_id', 'exist', 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    protected function rulesStatus(): array
    {
        return [
            ['status', 'required'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
        ];
    }

    protected function rulesCustomerName(): array
    {
        return [
            ['customer_full_name', 'required'],
            ['customer_full_name', 'string', 'max' => 256],
        ];
    }

    protected function rulesPhone(): array
    {
        return [
            ['phone', 'required'],
            ['phone', 'string', 'max' => 256],
        ];
    }

    protected function rulesPrice(): array
    {
        return [
            ['phone', 'required'],
            ['phone', 'string', 'max' => 256],
        ];
    }

    protected function rulesComment(): array
    {
        return [
            ['comment', 'required'],
            ['comment', 'string', 'max' => 1024],
        ];
    }
}
