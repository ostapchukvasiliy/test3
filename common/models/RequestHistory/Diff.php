<?php

namespace common\models\RequestHistory;

class Diff
{
    private ?string $name;
    private ?int $status;
    private ?string $status_name;
    private ?string $customer_full_name;
    private ?string $phone;
    private ?int $created_at;
    private ?float $price;
    private ?int $product_id;
    private ?string $product_name;
    private ?string $comment;

    public static function fromArray(array $array): self
    {
        $diff = new self;
        $diff->setName($array['name'] ?? null);
        $diff->setStatus($array['status'] ?? null);
        $diff->setStatusName($array['status_name'] ?? null);
        $diff->setCustomerFullName($array['customer_full_name'] ?? null);
        $diff->setPhone($array['phone'] ?? null);
        $diff->setPrice($array['price'] ?? null);
        $diff->setProductId($array['product_id'] ?? null);
        $diff->setProductName($array['product_name'] ?? null);
        $diff->setComment($array['comment'] ?? null);
        $diff->setCreatedAt($array['created_at'] ?? null);

        return $diff;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'status' => $this->status,
            'status_name' => $this->status_name,
            'customer_full_name' => $this->customer_full_name,
            'phone' => $this->phone,
            'price' => $this->price,
            'product_id' => $this->product_id,
            'product_name' => $this->product_name,
            'comment' => $this->comment,
            'created_at' => $this->created_at,
        ];
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getStatusName(): ?string
    {
        return $this->status_name;
    }

    /**
     * @return string|null
     */
    public function getCustomerFullName(): ?string
    {
        return $this->customer_full_name;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return int|null
     */
    public function getCreatedAt(): ?int
    {
        return $this->created_at;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return int|null
     */
    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    /**
     * @return string|null
     */
    public function getProductName(): ?string
    {
        return $this->product_name;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @param string|null $status_name
     */
    public function setStatusName(?string $status_name): void
    {
        $this->status_name = $status_name;
    }

    /**
     * @param string|null $customer_full_name
     */
    public function setCustomerFullName(?string $customer_full_name): void
    {
        $this->customer_full_name = $customer_full_name;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @param int|null $created_at
     */
    public function setCreatedAt(?int $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param int|null $product_id
     */
    public function setProductId(?int $product_id): void
    {
        $this->product_id = $product_id;
    }

    /**
     * @param string|null $product_name
     */
    public function setProductName(?string $product_name): void
    {
        $this->product_name = $product_name;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }
}
