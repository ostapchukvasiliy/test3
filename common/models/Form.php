<?php

namespace common\models;

use common\exceptions\InvalidFormException;
use yii\base\Model;

class Form extends Model
{
    /**
     * @throws InvalidFormException
     */
    public static function validateData(array $data, array $attrs = [])
    {
        $form = new static($attrs);
        $form->load($data);
        $form->validateWithException();
    }

    public function validateWithException()
    {
        if (! $this->validate()) {
            throw new InvalidFormException($this);
        }
    }

    public function formName(): string
    {
        return '';
    }
}