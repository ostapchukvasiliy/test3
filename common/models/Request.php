<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $comment
 * @property string $customer_full_name
 * @property string $name
 * @property string $phone
 * @property float|null $price
 * @property int $product_id
 * @property int $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property Product $product
 * @property RequestHistory[] $history
 */
class Request extends ActiveRecord
{
    public const STATUS_NEW = 1;
    public const STATUS_ACCEPTED = 2;
    public const STATUS_REJECTED = 3;
    public const STATUS_DEFECT = 4;

    public const STATUS_LIST = [
        self::STATUS_NEW => 'Новая',
        self::STATUS_ACCEPTED => 'Принята',
        self::STATUS_REJECTED => 'Отказана',
        self::STATUS_DEFECT => 'Брак',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%request}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            ['comment', 'required'],
            ['comment', 'string', 'max' => 1024],

            ['name', 'string', 'max' => 256],
            ['name', 'filter', 'skipOnError' => false, 'filter' => fn($value) => $value === '' ? null : $value],

            ['phone', 'required'],
            ['phone', 'string', 'max' => 256],

            ['price', 'double'],
            ['price', 'filter', 'skipOnError' => false, 'filter' => fn($value) => $value === '' ? null : (float) $value],

            ['customer_full_name', 'required'],
            ['customer_full_name', 'string', 'max' => 256],

            ['product_id',  'required'],
            ['product_id', 'exist', 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            ['product_id', 'filter', 'skipOnError' => false, 'filter' => fn($value) => $value === '' ? null : (int) $value],

            ['status', 'required'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::STATUS_LIST)],
            ['status', 'filter', 'skipOnError' => false, 'filter' => fn($value) => $value === '' ? null : (int) $value],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id'                 => 'Ид.',
            'customer_full_name' => 'Клиент',
            'name'               => 'Наименование',
            'phone'              => 'Телефон',
            'price'              => 'Цена',
            'product'            => 'Продукт',
            'product_id'         => 'Продукт',
            'comment'            => 'Комментарий',
            'status'             => 'Статус',
            'created_at'         => 'Дата создания',
            'updated_at'         => 'Дата изменения',
        ];
    }

    public function statusName(): string
    {
        return self::STATUS_LIST[$this->status] ?? '';
    }

    public function formName(): string
    {
        return '';
    }

    public function changeStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNextStatuses(): array
    {
        return array_filter(
            array_keys(self::STATUS_LIST),
            fn($status) => !in_array($status, [self::STATUS_NEW, $this->status])
        );
    }

    public function getProduct(): ActiveQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    public function getRequestHistory(): ActiveQuery
    {
        return $this->hasMany(RequestHistory::class, ['id' => 'request_id']);
    }
}
