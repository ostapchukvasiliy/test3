<?php

namespace common\models;

use common\models\RequestHistory\Diff;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $request_id
 * @property integer $user_id
 * @property array $diff
 * @property integer $created_at
 * @property User $user
 * @property Request $request
 */
class RequestHistory extends ActiveRecord
{
    private ?Diff $fillDiff = null;

    public static function tableName(): string
    {
        return '{{%request_history}}';
    }

    public static function completeDiff(array $diff): array
    {
        if ($diff['status'] ?? null) {
            $diff['status_name'] = Request::STATUS_LIST[$diff['status']];
        }
        if ($diff['product_id'] ?? null) {
            $diff['product_name'] = Product::findOne(['id' => $diff['product_id']])->name;
        }
        return $diff;
    }

    public function behaviors(): array
    {
        return [
            new TimestampBehavior([
                'updatedAtAttribute' => null,
            ]),
        ];
    }

    public function rules(): array
    {
        return [
            ['request_id',  'required'],
            ['request_id', 'exist', 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'id']],

            ['user_id',  'required'],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id'         => 'Ид.',
            'request_id' => 'Заявка',
            'user_id'    => 'Пользователь',
            'diff'       => 'Изменения',
            'created_at' => 'Дата создания',
        ];
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getRequest(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'request_id']);
    }

    public function getFillDiff(): Diff
    {
        if ($this->fillDiff === null) {
            $diff = is_array($this->diff) ? $this->diff : [];
            $this->fillDiff = Diff::fromArray($diff);
        }

        return $this->fillDiff;
    }
}
