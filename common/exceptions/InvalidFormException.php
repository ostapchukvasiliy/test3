<?php

namespace common\exceptions;

use yii\base\Exception as BaseException;
use yii\base\Model;
use Throwable;

class InvalidFormException extends BaseException
{
    private Model $form;

    public function __construct(Model $form, ?Throwable $previous = null)
    {
        $this->form = $form;

        parent::__construct('Invalidate form ' . $form::class . '.', 0, $previous);
    }

    public function getForm(): Model
    {
        return $this->form;
    }
}