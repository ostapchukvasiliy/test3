<?php

use common\helpers\Env;

$driver = Env::get('DB_CONNECTION');
$host = Env::get('DB_HOST');
$port = Env::get('DB_PORT');
$name =  Env::get('DB_DATABASE');
$dbUri = "$driver:host=$host;port=$port;dbname=$name";

return [
    'name' => 'Test',
    'language' => 'ru-RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
        'db' => [
            'class' => \yii\db\Connection::class,
            'dsn' => $dbUri,
            'username' => Env::get('DB_USER'),
            'password' => Env::get('DB_PASSWORD'),
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'viewPath' => '@common/mail',
            // send all mails to a file by default.
            'useFileTransport' => Env::get('MAIL_TRANSPORT') === 'file',
            'transport' => [
                'scheme' => 'smtps',
                'host' => Env::get('SMTP_HOST'),
                'username' => Env::get('SMTP_USERNAME'),
                'password' => Env::get('SMTP_PASSWORD'),
                'port' => Env::get('SMTP_PORT'),
                'encryption' => Env::get('SMTP_ENCRYPTION'),
            ],
        ],
        'authManager' => [
            'class' => yii\rbac\DbManager::class,
            'defaultRoles' => ['guest'],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm',
        ],
    ],
    'container' => [
        'definitions' => [
            \yii\widgets\LinkPager::class => \yii\bootstrap5\LinkPager::class,
        ],
    ],
];
