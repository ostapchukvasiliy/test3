<?php
return [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
        'user' => [
            'class' => \yii\web\User::class,
            'identityClass' => \common\models\User::class,
        ],
        'db' => [
            'dsn' => 'sqlite:@console/runtime/test.db',
        ],
    ],
];
