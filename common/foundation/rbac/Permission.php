<?php

namespace common\foundation\rbac;

class Permission extends \yii\rbac\Permission
{
    public const USER_VIEW = 'user.view';
    public const USER_CREATE = 'user.create';
    public const USER_UPDATE = 'user.update';
    public const USER_DELETE = 'user.delete';

    public const REQUEST_VIEW = 'request.view';
    public const REQUEST_CREATE = 'request.create';
    public const REQUEST_UPDATE = 'request.update';
    public const REQUEST_DELETE = 'request.delete';

    public const REQUEST_HISTORY_VIEW = 'request.history.view';
}
