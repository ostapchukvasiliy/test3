<?php

namespace common\foundation\rbac;

use Yii;
use yii\rbac\Role as RbacRole;

class Role extends RbacRole
{
    public const ADMIN = 'admin';
    public const MANAGER = 'manager';

    public static function getActiveList(): array
    {
        $auth = Yii::$app->authManager;

        $roles = array_filter(
            $auth->getRoles(),
            fn(RbacRole $role) => ! in_array($role->name, $auth->getDefaultRoles())
        );

        return array_map(fn(RbacRole $role) => $role->description, $roles);
    }
}
