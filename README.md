# Require:
    - Install `docker` 
    - Install `docker-compose` 

# Create `.env` file
    cp .env.dev .env

# Run docker containers
    docker-compose up -d

# Grant access for local folder
    sudo chown -R $USER:$USER ~/.composer-docker

# Go into container `backend`
    docker-compose exec backend bash

# Install composer packages
    composer install

# Set permission
    chmod 0777 console/runtime
    chmod 0777 backend/runtime
    chmod 0777 frontend/runtime

# Run migrations
    ./yii migrate
    ./yii migrate --migrationPath=@yii/rbac/migrations

# Init application
    ./yii init/generate-app-key
    ./yii rbac/fill
    ./yii product/fill
    ./yii user/create-admin admin admin@mail.test 123456
    ./yii user/demo 30