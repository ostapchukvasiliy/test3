<?php

namespace frontend\controllers;

use common\services\RequestService;
use common\exceptions\InvalidFormException;
use common\models\Request;
use frontend\models\RequestForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class RequestController extends Controller
{
    private RequestService $service;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->service = new RequestService();
    }

    public function actionIndex(): Response|string
    {
        $form = new RequestForm;
        if ($this->request->isPost) {
            $form->load($this->request->post());
            if ($form->validate() && $this->createRequest()) {
                Yii::$app->session->setFlash('success', 'Ваша заявка успешно отправлена.');
                return $this->redirect('/request');
            }
            Yii::$app->session->setFlash('error', 'Ошибка отправки формы.');
        }

        return $this->render('form', [
            'request' => $form,
        ]);
    }

    private function createRequest(): bool
    {
        $data = $this->request->post();
        $data['status'] = Request::STATUS_NEW;

        try {
            $this->service->create($data);
        } catch (InvalidFormException) {
            return false;
        }
        return true;
    }
}
