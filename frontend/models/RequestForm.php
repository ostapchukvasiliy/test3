<?php

namespace frontend\models;

class RequestForm extends \common\models\RequestForm
{
    public function rules(): array
    {
        return [
            ...$this->rulesComment(),
            ...$this->rulesCustomerName(),
            ...$this->rulesPhone(),
            ...$this->rulesProductId(),
        ];
    }

    public function attributeLabels(): array
    {
        return [
            ...parent::attributeLabels(),
            'customer_full_name' => 'ФИО',
        ];
    }
}
