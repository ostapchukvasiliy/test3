<?php

require __DIR__ . '/../../bootstrap/app.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php',
    require __DIR__ . '/../config/main.php'
);

/** @noinspection PhpUnhandledExceptionInspection */
(new yii\web\Application($config))->run();
