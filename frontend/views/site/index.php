<?php

/** @var yii\web\View $this */

$this->title = 'Главная';
?>
<div class="site-index">
    <div class="p-5 mb-4 bg-transparent rounded-3">
        <div class="container-fluid py-5 text-center">
            <h1 class="display-4 mb-5">Тестовое задание!</h1>
            <p><a class="btn btn-lg btn-success" href="/request">Подать заявку</a></p>
        </div>
    </div>
</div>
