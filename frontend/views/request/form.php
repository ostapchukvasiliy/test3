<?php

/** @var yii\web\View $this */
/* @var RequestForm $request */

use frontend\models\RequestForm;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$this->title = 'Заявка';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title; ?></h1>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($request, 'customer_full_name')->textInput(); ?>
    <?= $form->field($request, 'product_id')->dropDownList($request->getProductList()); ?>
    <?= $form->field($request, 'phone')->textInput(); ?>
    <?= $form->field($request, 'comment')->textarea(); ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить заявку', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>