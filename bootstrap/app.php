<?php

require __DIR__ . '/../vendor/autoload.php';

$repository = Dotenv\Repository\RepositoryBuilder::createWithNoAdapters()
    ->addAdapter(Dotenv\Repository\Adapter\EnvConstAdapter::class)
    ->addWriter(Dotenv\Repository\Adapter\PutenvAdapter::class)
    ->immutable()
    ->make();

Dotenv\Dotenv::create($repository, dirname(__DIR__))->load();

defined('YII_DEBUG') or define('YII_DEBUG', filter_var(getenv('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN));
defined('YII_ENV') or define('YII_ENV', getenv('APP_ENV') ?: 'prod');
defined('APP_PATH') or define('APP_PATH', getenv('APP_PATH') ?: dirname(__DIR__));

require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../common/config/bootstrap.php';
