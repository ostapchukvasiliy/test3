<?php

use common\models\Request;
use common\models\RequestHistory;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var Request $request */
/* @var RequestHistory[] $data */
/* @var array $pager */

$this->title = "История изменений заявки #$request->id";
$this->params['breadcrumbs'][] = ['url' => '/request', 'label' => 'Заявки'];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title; ?></h1>
<div class="user-default-index">
    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider(['allModels' => $data]),
        'columns' => [
            [
                'label' => 'Ип.',
                'attribute' => 'id',
            ],
            [
                'label' => 'Статус',
                'value' => fn(RequestHistory $history) => $history->getFillDiff()->getStatusName() ?: '',
            ],
            [
                'label' => 'Наименование',
                'value' => fn(RequestHistory $history) => $history->getFillDiff()->getName() ?: '',
            ],
            [
                'label' => 'ФИО Клиента',
                'value' => fn(RequestHistory $history) => $history->getFillDiff()->getCustomerFullName() ?: ''
            ],
            [
                'label' => 'Телефон',
                'value' => fn(RequestHistory $history) =>  $history->getFillDiff()->getPhone() ?: ''
            ],
            [
                'label' => 'Дата',
                'value' => function(RequestHistory $history) {
                    $time = $history->getFillDiff()->getCreatedAt();
                    if (! $time) {
                        return '';
                    }
                    return \Yii::$app->formatter->asDatetime($time);
                },
            ],
            [
                'label' => 'Цена',
                'value' => fn(RequestHistory $history) => $history->getFillDiff()->getPrice() ?? '',
            ],
            [
                'label' => 'Продукт',
                'value' => fn(RequestHistory $history) => $history->getFillDiff()->getProductName() ?? '',
            ],
            [
                'label' => 'Комментарий',
                'value' => fn(RequestHistory $history) => $history->getFillDiff()->getComment() ?? '',
            ],
            [
                'label' => 'Пользователь',
                'attribute' => 'user.username',
            ],
            [
                'label' => 'Дата изменения',
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
        ],
    ]); ?>
</div>
