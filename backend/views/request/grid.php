<?php

use common\foundation\rbac\Permission;
use common\models\Request;
use yii\bootstrap5\ButtonDropdown;
use yii\data\ArrayDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;

/* @var Request[] $data */
/* @var array $pager */

$this->title = 'Список заявок';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title; ?></h1>
<div class="user-default-index">
    <?php if (Yii::$app->user->can(Permission::REQUEST_CREATE)): ?>
        <div class="pt-1 pb-2">
            <?php echo Html::a('Создать заявку', ['/request/create'], [
                'class' => 'btn btn-primary grid-button'
            ]); ?>
            <?php echo Html::a('Экспорт СSV', ['/request/export-csv'], [
                'class' => 'btn btn-default grid-button',
                'download'
            ]); ?>
        </div>
    <?php endif; ?>
    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider(['allModels' => $data]),
        'columns' => [
            ['class' => SerialColumn::class],

            [
                'label' => 'Ид.',
                'attribute' => 'id',
            ],
            [
                'label' => 'Наименование',
                'attribute' => 'name',
            ],
            [
                'label' => 'Клиент',
                'attribute' => 'customer_full_name',
            ],
            [
                'label' => 'Телефон',
                'attribute' => 'phone',
            ],
            [
                'label' => 'Комментарий',
                'attribute' => 'comment',
                'format' => 'ntext',
            ],
            [
                'label' => 'Цена',
                'attribute' => 'price',
            ],
            [
                'attribute' => 'status',
                'value' => fn(Request $request) => $request->statusName(),
            ],
            [
                'label' => 'Продукт',
                'value' => fn(Request $request) => $request->product->name,
            ],
            [
                'label' => 'Дата создания',
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
            [
                'class' => ActionColumn::class,
                'visibleButtons' => [
                    'view' => Yii::$app->user->can(Permission::REQUEST_VIEW),
                    'update' => Yii::$app->user->can(Permission::REQUEST_UPDATE),
                    'delete' => Yii::$app->user->can(Permission::REQUEST_DELETE),
                    'change-status' => Yii::$app->user->can(Permission::REQUEST_UPDATE),
                    'history' => Yii::$app->user->can(Permission::REQUEST_HISTORY_VIEW),
                ],
                'urlCreator' => fn($action, $model) => [$action, 'id' => $model['id']],
                'template' => '{view} {update} {delete} {history} {change-status}',
                'buttons' => [
                    'history' => fn($url) => Html::a(
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;width:1.125em"><path fill="currentColor" d="M448 360V24c0-13.3-10.7-24-24-24H96C43 0 0 43 0 96v320c0 53 43 96 96 96h328c13.3 0 24-10.7 24-24v-16c0-7.5-3.5-14.3-8.9-18.7-4.2-15.4-4.2-59.3 0-74.7 5.4-4.3 8.9-11.1 8.9-18.6zM128 134c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm0 64c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm253.4 250H96c-17.7 0-32-14.3-32-32 0-17.6 14.4-32 32-32h285.4c-1.9 17.1-1.9 46.9 0 64z"/></svg>',
                        $url,
                        ['title' => 'История изменения']
                    ),
                    'change-status' => fn($url, Request $model) => ButtonDropdown::widget([
                        'label' => 'Изменить статус',
                        'dropdown' => [
                            'items' => array_map(
                                fn($status) => [
                                    'label' => Request::STATUS_LIST[$status],
                                    'url' => $url + ['status' => $status],
                                ],
                                $model->getNextStatuses()
                            )
                        ],
                    ]),
                ]
            ],
        ],
    ]); ?>
</div>
