<?php

/** @var yii\web\View $this */
/* @var RequestForm $request */

use common\models\RequestForm;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$title = !$request->id ? 'Создание заявки' : "Редактирование заявки #$request->id";
$buttonText = !$request->id ? 'Создать заявку' : "Сохранить изменения";

$this->title = "$title";
$this->params['breadcrumbs'][] = ['url' => '/request', 'label' => 'Заявки'];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title; ?></h1>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($request, 'name')->textInput(); ?>
    <?= $form->field($request, 'customer_full_name')->textInput(); ?>
    <?= $form->field($request, 'status')->dropDownList($request->getStatusList()); ?>
    <?= $form->field($request, 'product_id')->dropDownList($request->getProductList()); ?>
    <?= $form->field($request, 'price')->textInput(); ?>
    <?= $form->field($request, 'phone')->textInput(); ?>
    <?= $form->field($request, 'comment')->textarea(); ?>

    <div class="form-group">
        <?= Html::submitButton($buttonText, ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>