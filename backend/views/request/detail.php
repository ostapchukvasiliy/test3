<?php

use common\models\Request;
use yii\widgets\DetailView;

/** @var Request $request */
/** @var yii\web\View $this */

$this->title = "Заявка #$request->id";
$this->params['breadcrumbs'][] = ['url' => '/request', 'label' => 'Заявки'];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title; ?></h1>
<div class="user-default-index">
    <?php echo DetailView::widget([
        'model' => $request,
        'attributes' => [
            'id',
            'name',
            'customer_full_name',
            'phone',
            'comment:ntext',
            'price',
            [
                'attribute' => 'status',
                'value' => fn(Request $request) => $request->statusName(),
            ],
            [
                'label' => 'Продукт',
                'value' => fn(Request $request) => $request->product->name,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]); ?>
</div>
