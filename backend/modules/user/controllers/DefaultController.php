<?php

namespace backend\modules\user\controllers;

use common\exceptions\InvalidFormException;
use backend\modules\user\models\CreateUserForm;
use backend\modules\user\models\UpdateUserForm;
use backend\modules\user\services\UserService;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{
    private UserService $service;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->service = new UserService();
    }

    public function actionIndex(): string
    {
        $data = [
            'perPage' => $this->request->get('per-page'),
            'page' => $this->request->get('page'),
            'sortBy' => $this->request->get('sort-by'),
            'sortOrder' => $this->request->get('sort-order'),
        ];

        return $this->render('grid', $this->service->list($data));
    }

    public function actionView(?int $id): string|Response
    {
        return $this->render('detail', [
            'user' => $this->service->get($id)
        ]);
    }

    public function actionCreate(): string|Response
    {
        if ($this->request->isGet) {
            return $this->render('form', ['user' => new CreateUserForm]);
        }
        try {
            $this->service->create(
                $this->request->post()
            );
        } catch (InvalidFormException $e) {
            return $this->render('form', ['user' => $e->getForm()]);
        }

        return $this->redirect(['index']);
    }

    public function actionUpdate(int $id): string|Response
    {
        $form = UpdateUserForm::createByUser(
            $this->service->get($id)
        );
        if ($this->request->isGet) {
            return $this->render('form', ['user' => $form]);
        }

        try {
            $this->service->update(
                $id,
                $this->request->post()
            );
        } catch (InvalidFormException $e) {
            return $this->render('form', ['user' => $e->getForm()]);
        }

        return $this->redirect(['index']);
    }

    public function actionDelete(int $id): Response
    {
        $this->service->delete($id);

        return $this->redirect(['index']);
    }

    public function actionActivate(int $id): Response
    {
        $this->service->activate($id);

        return $this->redirect(['index']);
    }

    public function actionDeactivate(int $id): Response
    {
        $this->service->deactivate($id);

        return $this->redirect(['index']);
    }
}
