<?php

namespace backend\modules\user\models;

class CreateUserForm extends UserForm
{
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->setScenario(self::SCENARIO_CREATE);
    }
}