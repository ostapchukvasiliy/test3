<?php

namespace backend\modules\user\models;

use common\foundation\rbac\Role;
use common\models\Form;
use common\models\User;

class UserForm extends Form
{
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';

    public ?int $id = null;
    public string $username = '';
    public string $password = '';
    public string $email = '';
    public int $status = User::STATUS_ACTIVE;
    public ?array $roles = [];

    public static function createByUser(User $user): UserForm
    {
        $form = new self();
        $form->id = $user->id;
        $form->username = $user->username;
        $form->email = $user->email;
        $form->status = $user->status;
        $form->roles = $user->roleNames();

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            ['username', 'required'],
            [
                'username',
                'match',
                'pattern' => '/^[0-9a-zA-Z_-]/i',
            ],
            [
                'username',
                fn($attr) => $this->validateEqualAttribute($attr),
                'message' => 'Такой логин уже используется другим пользователем.',
            ],

            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                fn($attr) => $this->validateEqualAttribute($attr),
                'message' => 'Такой эл. адрес уже используется другим пользователем.',
            ],

            ['password', 'required', 'on' => self::SCENARIO_CREATE, 'except' => self::SCENARIO_UPDATE],
            ['password', 'string', 'min' => 6],

            ['status', 'required'],
            ['status', 'default', 'value' => User::STATUS_INACTIVE],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],

            ['roles', 'required'],
            ['roles', 'each', 'rule' => ['in', 'range' => array_keys($this->getRolesList())]],
        ];
    }

    public function getStatusList(): array
    {
        return User::STATUS_LIST;
    }

    public function getRolesList(): array
    {
        return Role::getActiveList();
    }

    public function attributeLabels(): array
    {
        return [
            'id'       => 'Ид.',
            'username' => 'Логин',
            'password' => 'Пароль',
            'email'    => 'Эл. адрес',
            'status'   => 'Статус',
            'roles'    => 'Роли',
        ];
    }

    protected function validateEqualAttribute(string $attr): bool
    {
        $query = User::find()->where([$attr => $this->username]);
        if ($this->id) {
            $query->where(['!=', 'id', $this->id]);
        }
        return $query->exists();
    }
}