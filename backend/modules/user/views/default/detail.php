<?php
use common\models\User;
use yii\widgets\DetailView;

/* @var User $user */

$this->title = "Пользователь #$user->id";
$this->params['breadcrumbs'][] = ['url' => '/request', 'label' => 'Заявки'];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title ; ?></h1>
<div class="user-default-index">
    <?php echo DetailView::widget([
        'model' => $user,
        'attributes' => [
            'id',
            'username',
            'email',
            [
                'attribute' => 'status',
                'value' => fn(User $user) => $user->statusName(),
            ],
            [
                'label' => 'Роли',
                'value' => fn(User $user) => implode(', ', $user->roleLabels()),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]); ?>
</div>
