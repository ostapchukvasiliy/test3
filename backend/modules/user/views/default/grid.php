<?php

use common\foundation\rbac\Permission;
use common\models\User;
use yii\data\ArrayDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;

/* @var User[] $data */
/* @var array $pager */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?php echo $this->title; ?></h1>
<div class="user-default-index">
    <div class="pt-1 pb-2">
    <?php if (Yii::$app->user->can(Permission::USER_CREATE)): ?>
        <?php echo Html::a('Создать пользователя', ['/user/default/create'], [
            'class' => 'btn btn-primary grid-button mb-1'
        ]); ?>
    <?php endif; ?>
    </div>
    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider(['allModels' => $data]),
        'columns' => [
            ['class' => SerialColumn::class],

            'id',
            'username',
            'email',
            [
                'attribute' => 'status',
                'value' => fn(User $user) => $user->statusName(),
            ],
            [
                'label' => 'Роли',
                'value' => fn(User $user) => implode(', ', $user->roleLabels()),
            ],

            [
                'class' => ActionColumn::class,
                'visibleButtons' => [
                    'view' => Yii::$app->user->can(Permission::USER_VIEW),
                    'update' => Yii::$app->user->can(Permission::USER_UPDATE),
                    'delete' => Yii::$app->user->can(Permission::USER_DELETE),
                    'activate' => fn(User $user) => $user->isInactive() &&
                        Yii::$app->user->can(Permission::USER_UPDATE),
                    'deactivate' => fn(User $user) => $user->isActive() &&
                        Yii::$app->user->can(Permission::USER_UPDATE),
                ],
                'urlCreator' => fn($action, $model) => [$action, 'id' => $model['id']],
                'template' => '{view} {activate} {deactivate} {update} {delete}',
                'buttons' => [
                    'activate' => fn($url) => Html::a(
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;width:1.125em"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"/></svg>',
                        $url,
                        ['title' => 'Активировать']
                    ),
                    'deactivate' => fn($url) => Html::a(
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;width:1.125em"><path fill="currentColor" d="M256 8C119.034 8 8 119.033 8 256s111.034 248 248 248 248-111.034 248-248S392.967 8 256 8zm130.108 117.892c65.448 65.448 70 165.481 20.677 235.637L150.47 105.216c70.204-49.356 170.226-44.735 235.638 20.676zM125.892 386.108c-65.448-65.448-70-165.481-20.677-235.637L361.53 406.784c-70.203 49.356-170.226 44.736-235.638-20.676z"/></svg>',
                        $url,
                        ['title' => 'Деактивировать']
                    ),
                ]
            ],
        ],
    ]); ?>
</div>
