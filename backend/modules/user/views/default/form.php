<?php

use backend\modules\user\models\UserForm;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

/* @var UserForm $user */
$title = !$user->id ? 'Создание пользователя' : "Редактирование пользователя #$user->id";
$buttonText = !$user->id ? 'Создать пользователя' : "Сохранить изменения";
$this->title = $title;
$this->params['breadcrumbs'][] = ['url' => '/request', 'label' => 'Заявки'];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="card-title pt-1 pb-4"><?= $title; ?></h1>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($user, 'username')->textInput(); ?>
    <?= $form->field($user, 'email')->textInput(); ?>
    <?= $form->field($user, 'status')->dropDownList($user->getStatusList()); ?>
    <?= $form->field($user, 'password')->passwordInput(); ?>
    <?= $form->field($user, 'roles')->dropDownList($user->getRolesList(), ['multiple' => true, 'unselect' => null]); ?>

    <div class="form-group">
        <?= Html::submitButton($buttonText, ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>