<?php

namespace backend\modules\user\services;

use common\exceptions\InvalidFormException;
use backend\modules\user\models\CreateUserForm;
use backend\modules\user\models\UpdateUserForm;
use common\foundation\rbac\Permission;
use common\models\User;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\data\Pagination;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use Yii;
use Throwable;

class UserService
{
    private bool $checkAccess = true;

    const DEFAULT_PER_PAGE = 25;
    const DEFAULT_PAGE = 1;
    const DEFAULT_SORT_BY = 'id';
    const DEFAULT_SORT_ORDER = 'desc';

    /**
     * @throws UnauthorizedHttpException
     */
    public function list(array $data = []): array
    {
        $this->checkAccess(Permission::USER_VIEW);

        $perPage = ((int) $data['perPage'] ?? self::DEFAULT_PER_PAGE);
        $page = ((int) $data['page'] ?? self::DEFAULT_PAGE);
        $sortBy = $data['sort'] ?? self::DEFAULT_SORT_BY;
        $sortOrder = $data['sort'] ?? self::DEFAULT_SORT_ORDER;

        $query = User::find();
        $pager = new Pagination([
            'totalCount' => (clone $query)->count()
        ]);
        $pager->setPage($page);
        $pager->setPageSize($perPage);

        $users = $query
            ->orderBy([$sortBy => $sortOrder])
            ->offset($pager->getOffset())
            ->limit($pager->getLimit())
            ->all();

        return [
            'data' => $users,
            'pager' => (array) $pager,
        ];
    }

    /**
     * @throws UnauthorizedHttpException
     * @throws NotFoundHttpException
     */
    public function get(?int $id): User
    {
        $this->checkAccess(Permission::USER_VIEW);

        return $this->getById($id);
    }

    /**
     * @throws UnauthorizedHttpException
     * @throws InvalidFormException
     * @throws Exception
     */
    public function create(array $data): void
    {
        $this->checkAccess(Permission::USER_CREATE);

        CreateUserForm::validateData($data);

        $password = $data['password'] ?? null;
        unset($data['password']);

        $user = new User();
        $user->load($data);
        $user->generateAuthKey();
        $user->setPassword($password);
        $this->save($user);
        $this->syncRoles($user, $data['roles']);
    }


    /**
     * @throws UnauthorizedHttpException
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function update(?int $id, array $data): void
    {
        $this->checkAccess(Permission::USER_UPDATE);
        UpdateUserForm::validateData($data, ['id' => $id]);

        $user = $this->getById($id);
        $user->load($data);
        if (isset($data['password']))  {
            $user->setPassword($data['password']);
        }
        $this->save($user);
        $this->syncRoles($user, $data['roles']);
    }

    /**
     * @throws UnauthorizedHttpException
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function activate(?int $id): void
    {
        $this->checkAccess(Permission::USER_UPDATE);
        $user = $this->getById($id);
        $user->activate();
        $this->save($user);
    }

    /**
     * @throws Exception
     * @throws UnauthorizedHttpException
     * @throws NotFoundHttpException
     */
    public function deactivate(?int $id): void
    {
        $this->checkAccess(Permission::USER_UPDATE);
        $user = $this->getById($id);
        $user->deactivate();
        $this->save($user);
    }

    /**
     * @throws StaleObjectException
     * @throws UnauthorizedHttpException
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function delete(?int $id): void
    {
        $this->checkAccess(Permission::USER_DELETE);
        $user = $this->getById($id);
        $user->delete();
    }

    public function skipCheckAccess($skip = true): self
    {
        $this->checkAccess = ! $skip;

        return $this;
    }

    /**
     * @throws UnauthorizedHttpException
     */
    private function checkAccess(string $permission): void
    {
        if (! $this->checkAccess) {
            return;
        }
        if (! Yii::$app->user->can($permission)) {
            throw new UnauthorizedHttpException('Нет доступа.');
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    private function getById(?int $id): User
    {
        $user = $id ? User::findOne(['id' => $id]) : null;
        if (! $user) {
            throw new NotFoundHttpException('Пользователь не найден.');
        }
        return $user;
    }

    /**
     * @throws Exception
     */
    private function save(User $user): void
    {
        if (! $user->validate()) {
            $errors = $user->getFirstErrors();
            throw new InvalidArgumentException(reset($errors));
        }
        if (! $user->save()) {
            throw new Exception('Ошибка сохранения.');
        }
    }

    private function syncRoles(User $user, array $roles): void
    {
        $user->syncRoles($roles);
    }
}