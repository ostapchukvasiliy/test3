<?php

namespace backend\controllers;

use common\models\RequestForm;
use common\exceptions\InvalidFormException;
use common\services\RequestService;
use yii\web\Controller;
use yii\web\Response;

class RequestController extends Controller
{
    private RequestService $service;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->service = new RequestService();
    }

    public function actionIndex(): string
    {
        $data = [
            'perPage' => $this->request->get('per-page'),
            'page' => $this->request->get('page'),
            'sortBy' => $this->request->get('sort-by'),
            'sortOrder' => $this->request->get('sort-order'),
        ];

        return $this->render('grid', $this->service->list($data));
    }

    public function actionView(?int $id): string|Response
    {
        return $this->render('detail', [
            'request' => $this->service->get($id)
        ]);
    }

    public function actionCreate(): string|Response
    {
        if ($this->request->isGet) {
            return $this->render('form', ['request' => new RequestForm]);
        }
        try {
            $this->service->create(
                $this->request->post()
            );
        } catch (InvalidFormException $e) {
            return $this->render('form', ['request' => $e->getForm()]);
        }

        return $this->redirect(['request/index']);
    }

    public function actionUpdate(int $id): string|Response
    {
        $form = RequestForm::createByRequest(
            $this->service->get($id)
        );
        if ($this->request->isGet) {
            return $this->render('form', ['request' => $form]);
        }

        try {
            $this->service->update(
                $id,
                $this->request->post()
            );
        } catch (InvalidFormException $e) {
            return $this->render('form', ['request' => $e->getForm()]);
        }

        return $this->redirect(['request/index']);
    }

    public function actionDelete(int $id): Response
    {
        $this->service->delete($id);

        return $this->redirect(['request/index']);
    }

    public function actionChangeStatus(int $id, int $status): Response
    {
        $this->service->changeStatus($id, $status);

        return $this->redirect(['request/index']);
    }

    public function actionHistory(int $id): string
    {
        $data = [
            'perPage' => $this->request->get('per-page'),
            'page' => $this->request->get('page'),
            'sortBy' => $this->request->get('sort-by'),
            'sortOrder' => $this->request->get('sort-order'),
        ];

        return $this->render('history', $this->service->history($id, $data));
    }

    public function actionExportCsv(): Response
    {
        return $this->response->sendContentAsFile($this->service->generateCsv(), 'export.csv', [
            'mimeType' => 'application/csv',
            'inline'   => false
        ]);
    }
}
